module Api
  module V1
    class MessagesController < ApplicationController
      respond_to :json
      protect_from_forgery prepend: true
      
      # GET /api/v1/messages
      def index
        messages = Message.all
        render json: MessageSerializer.new(messages).serializable_hash
      end

      # GET /api/v1/messages/:id
      def show
        messsage = Message.find(params[:id])
        render json: MessageSerializer.new(messsage).serializable_hash
      end

      # POST /api/v1/messages
      def create        
        messsage = Message.new(message_params)        
        if messsage.save            
          render json: { success: true, id: messsage.id }             
        else            
          render json: { error: true, description: messsage.errors.messages }             
        end        
      end

      private
        def message_params
          params.require(:message).permit(:email, :first_name, :last_name, :amount)
        end
    end
  end
end
