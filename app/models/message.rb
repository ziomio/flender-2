# == Schema Information
#
# Table name: messages
#
#  id         :bigint(8)        not null, primary key
#  email      :string
#  first_name :string
#  last_name  :string
#  amount     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Message < ApplicationRecord
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP } 
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :amount, presence: true, numericality: { only_integer: true }
end
