# == Schema Information
#
# Table name: messages
#
#  id         :bigint(8)        not null, primary key
#  email      :string
#  first_name :string
#  last_name  :string
#  amount     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class MessageSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :amount, :email
  attribute :name, &:first_name
  attribute :surname, &:last_name

  attribute :date do |object|
    object.created_at.in_time_zone('America/New_York').strftime("%m.%d.%Y %H:%M")    
  end  
end
