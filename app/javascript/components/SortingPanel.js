import React from "react"
import SelectWidget from "./SelectWidget"

class SortingPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sortType: this.props.sortType
    };

    this.updateSortType = this.updateSortType.bind(this);
  }

  updateSortType(newType) {
    this.setState({ sortType: newType });
    this.props.parentFn(newType);
  }

  render() {
    return (
      <div className="select-widget">
        Sort by <SelectWidget data={
          [
            ['by-first-name-asc', 'First Name ASC'],
            ['by-first-name-desc', 'First Name DESC'],
            ['by-amount-asc', 'amount ASC'],
            ['by-amount-desc', 'amount DESC']
          ]}
          default={this.state.sortType}
          parentFn={this.updateSortType} />
      </div>
    );
  }
}

export default SortingPanel
