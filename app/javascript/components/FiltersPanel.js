import React from "react"
import Filter from "./Filter"

class FiltersPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filters: {}
    };

    this.buildFilters = this.buildFilters.bind(this);
  }

  buildFilters(filter) {
    const newFilterTypes = { ...this.state.filters, ...filter };
    this.setState({ filters: newFilterTypes });
    this.props.parentFn(newFilterTypes);
  }

  render() {
    return (
      <div className="filters-panel">
        <Filter field="email" parentFn={this.buildFilters} />
        <Filter field="surname" parentFn={this.buildFilters} />
      </div>
    );
  }
}

export default FiltersPanel
