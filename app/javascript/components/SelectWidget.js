import React from "react"

class SelectWidget extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.default
    };

    this.informParent = this.informParent.bind(this);
  }

  informParent(event) {
    this.setState({ value: event.target.value });
    this.props.parentFn(event.target.value);
  }

  render() {
    const options = this.props.data.map((item, idx) => {
      return <option value={item[0]} key={idx}>{item[1]}</option>;
    });

    return (
      <select value={this.state.value} onChange={this.informParent}>
        {options}
      </select>
    );
  }
}

export default SelectWidget
