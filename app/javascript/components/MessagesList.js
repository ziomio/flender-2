import React from "react"
import equal from 'fast-deep-equal'
import Message from "./Message"


class MessagesList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
      isLoading: false,
      sortType: this.props.sortType,
      filterTypes: this.props.filterTypes
    };

    this.compareFn = this.compareFn.bind(this);
    this.filterFn = this.filterFn.bind(this);

  }

  componentDidUpdate(prevProps) {
    if (!equal(prevProps.sortType, this.props.sortType)) {
      this.setState({ sortType: this.props.sortType });
    }
    if (!equal(prevProps.filterTypes, this.props.filterTypes)) {
      this.setState({ filterTypes: this.props.filterTypes });
    }
  }

  componentDidMount() {
    this.setState({ isLoading: true });

    fetch("/api/v1/messages")
      .then(response => response.json())
      .then(json => this.setState({ messages: json.data, isLoading: false }));
  }

  filterFn(message) {
    for (const [name, filter] of Object.entries(this.state.filterTypes)) {
      // we are not interested in empty values
      if (filter.text.trim() == "") {
        continue;
      }

      if (message.attributes[name].toLowerCase().includes(
        filter.text.trim().toLowerCase())) {
        continue;
      }
      else {
        return false;
      }
    }
    return true;
  }

  compareFn(msg1, msg2) {
    switch (this.state.sortType) {
      case 'by-first-name-asc':
        return this.compareByFirstNameAsc(msg1, msg2);
      case 'by-first-name-desc':
        return this.compareByFirstNameDesc(msg1, msg2);
      case 'by-amount-asc':
        return this.compareByAmountAsc(msg1, msg2);
      case 'by-amount-desc':
        return this.compareByAmountDesc(msg1, msg2);
      default:
        throw new Error("Unknown sortType");
    }
  }

  compareByAmountAsc(msg1, msg2) {
    if (msg1.attributes.amount < msg2.attributes.amount) {
      return -1;
    }
    else if (msg1.attributes.amount > msg2.attributes.amount) {
      return 1;
    }
    return 0;
  }

  compareByAmountDesc(msg1, msg2) {
    if (msg1.attributes.amount > msg2.attributes.amount) {
      return -1;
    }
    else if (msg1.attributes.amount < msg2.attributes.amount) {
      return 1;
    }
    return 0;
  }

  compareByFirstNameAsc(msg1, msg2) {
    if (msg1.attributes.name < msg2.attributes.name) {
      return -1;
    }
    else if (msg1.attributes.name > msg2.attributes.name) {
      return 1;
    }
    return 0;
  }

  compareByFirstNameDesc(msg1, msg2) {
    if (msg1.attributes.name > msg2.attributes.name) {
      return -1;
    }
    else if (msg1.attributes.name < msg2.attributes.name) {
      return 1;
    }
    return 0;
  }

  render() {
    const { messages, isLoading } = this.state;
    if (isLoading) {
      return <p>Loading ...</p>;
    }

    return (
      <ul className="message-list">
        {messages.filter(this.filterFn).sort(this.compareFn).map((msg, idx) => {
          return <Message
            key={msg.id}
            name={msg.attributes.name}
            surname={msg.attributes.surname}
            amount={msg.attributes.amount}
            date={msg.attributes.date}
            email={msg.attributes.email} />;
        })}
      </ul>
    );
  }
}

export default MessagesList
