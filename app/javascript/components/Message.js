import React from "react"

class Message extends React.Component {
  render() {
    return (
      <li>
        {this.props.name} {this.props.surname}: {this.props.amount} USD, {this.props.date}, {this.props.email}
      </li>
    );
  }
}

export default Message
