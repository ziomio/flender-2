import React from "react"

class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: ''
    };

    this.setTextState = this.setTextState.bind(this);
  }


  setTextState(event) {
    this.setState({ text: event.target.value });
    this.props.parentFn({
      [this.props.field]: {
        text: event.target.value
      }
    });
  }

  render() {
    return (
      <div className="filter">
        <p>Filter by {this.props.field}</p>
        <input type="text" onChange={this.setTextState} />
      </div>
    );
  }
}

export default Filter
