import React from "react"
import SortingPanel from "./SortingPanel"
import MessagesList from "./MessagesList"
import FiltersPanel from "./FiltersPanel"

class MessagesDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // sortType can be:
      // by-first-name-asc
      // by-first-name-desc
      // by-amount-asc
      // by-amount-desc
      sortType: 'by-first-name-asc',
      filterTypes: {}
    };

    this.updateSortType = this.updateSortType.bind(this);
    this.updateFiltersType = this.updateFiltersType.bind(this);
  }

  updateSortType(newType) {
    this.setState({ sortType: newType });
  }

  updateFiltersType(newFilterTypes) {
    this.setState({ filterTypes: newFilterTypes });
  }

  render() {
    return (
      <div>
        <SortingPanel sortType={this.state.sortType} parentFn={this.updateSortType} />
        <FiltersPanel parentFn={this.updateFiltersType} />
        <MessagesList sortType={this.state.sortType} filterTypes={this.state.filterTypes} />
      </div>
    );
  }
}

export default MessagesDashboard
