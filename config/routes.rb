Rails.application.routes.draw do
  root 'home#home'

  namespace :api do
    namespace :v1 do
      resources :messages, only: [:create, :show, :index]
    end
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
